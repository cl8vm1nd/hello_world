FROM node:current-slim

ARG BUILD=1
ENV BUILD ${BUILD}

WORKDIR /var/www/app

COPY package*.json ./

RUN npm install --only=production

COPY . .

EXPOSE 80

CMD [ "npm", "start" ]

